﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour
{

	public PauseMenu pauseMenu;
	public Samurai LeftSamurai;
	public Samurai RightSamurai;
	public Menu player1Win;
	public Menu player2Win;

	// Use this for initialization
	void Start ()
	{
		GameSettings settings = GameSettings.GetInstance ();
		switch (settings.Player1Control) {
		case Samurai.CONTROL.AI:
			LeftSamurai.controller = new AIInput ();
			break;
		case Samurai.CONTROL.KEYBOARD:
			LeftSamurai.controller = new KeyboardInput ();
			break;
		case Samurai.CONTROL.XBOX_CONTROLLER:
			LeftSamurai.controller = new X360Controller ();
			break;
		}
		
		switch (settings.Player2Control) {
		case Samurai.CONTROL.AI:
			RightSamurai.controller = new AIInput ();
			break;
		case Samurai.CONTROL.KEYBOARD:
			RightSamurai.controller = new KeyboardInput ();
			break;
		case Samurai.CONTROL.XBOX_CONTROLLER:
			RightSamurai.controller = new X360Controller ();
			break;
		}

		LeftSamurai.controller.SetPlayer (1);
		LeftSamurai.controller.SetFlipped (false);

		RightSamurai.controller.SetPlayer (2);
		RightSamurai.controller.SetFlipped (true);
	}
	
	// Update is called once per frame
	void Update ()
	{
		GameSettings settings = GameSettings.GetInstance ();
		if (!settings.paused && (LeftSamurai.controller.Start () || RightSamurai.controller.Start ())) {
			settings.paused = true;
			pauseMenu.Show ();
		}

		if (!settings.paused) {
			if (LeftSamurai.Health <= 0) {
				settings.paused = true;
				player1Win.Show ();
			} else if (RightSamurai.Health <= 0) {
				settings.paused = true;
				player2Win.Show ();
			}
		}
	}
}
