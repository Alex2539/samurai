﻿using UnityEngine;
using System.Collections;

public class KeyboardInput : Controller
{

	private bool flipped = false;
	public void SetPlayer (int player)
	{

	}

	public void SetFlipped (bool flipped)
	{
		this.flipped = flipped;
	}

	public int Action ()
	{
		if (flipped) {
			if (Input.GetKey (KeyCode.LeftArrow))
				return 1;
			if (Input.GetKey (KeyCode.RightArrow))
				return -1;
		} else {
			if (Input.GetKey (KeyCode.LeftArrow))
				return -1;
			if (Input.GetKey (KeyCode.RightArrow))
				return 1;
		}
		return 0;
	}

	public bool HeavyDown ()
	{
		return Input.GetKeyDown (KeyCode.D);
	}

	public bool Heavy ()
	{
		return Input.GetKey (KeyCode.D);
	}

	public bool MediumDown ()
	{
		return Input.GetKeyDown (KeyCode.S);
	}

	public bool Medium ()
	{
		return Input.GetKey (KeyCode.S);
	}

	public bool LightDown ()
	{
		return Input.GetKeyDown (KeyCode.A);
	}

	public bool Light ()
	{
		return Input.GetKey (KeyCode.A);
	}

	public bool BlockDown ()
	{
		return Input.GetKeyDown (KeyCode.F);
	}
	
	public bool Block ()
	{
		return Input.GetKey (KeyCode.F);
	}

	public bool Start ()
	{
		return Input.GetKeyDown (KeyCode.Escape);
	}

	public bool Up ()
	{
		return Input.GetKeyDown (KeyCode.UpArrow);
	}

	public bool Down ()
	{
		return Input.GetKeyDown (KeyCode.DownArrow);
	}

	public bool Left ()
	{
		return Input.GetKeyDown (KeyCode.LeftArrow);
	}

	public bool Right ()
	{
		return Input.GetKeyDown (KeyCode.RightArrow);
	}

	public bool Accept ()
	{
		return Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.A);
	}

	public bool Back ()
	{
		return Input.GetKeyDown (KeyCode.F);
	}
}
