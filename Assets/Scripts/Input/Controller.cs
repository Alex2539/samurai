﻿using UnityEngine;
using System.Collections;

public interface Controller
{
	void SetPlayer (int player);
	void SetFlipped (bool flipped);
	int Action ();
	bool HeavyDown ();
	bool Heavy ();
	bool MediumDown ();
	bool Medium ();
	bool LightDown ();
	bool Light ();
	bool BlockDown ();
	bool Block ();
	bool Start ();
	bool Up ();
	bool Down ();
	bool Left ();
	bool Right ();
	bool Accept ();
	bool Back ();
}
