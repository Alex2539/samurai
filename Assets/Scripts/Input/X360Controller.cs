﻿using UnityEngine;
using System.Collections;

public class X360Controller : Controller
{

	private int ControllerNumber = 0;
	private bool flipped = false;

	private bool upDown = false;
	private bool downDown = false;
	private bool leftDown = false;
	private bool rightDown = false;

	public void SetFlipped (bool flipped)
	{
		this.flipped = flipped;
	}

	public void SetPlayer (int player)
	{
		ControllerNumber = player;
		if (ControllerNumber < 1 || ControllerNumber > 4)
			ControllerNumber = 0;
		if (ControllerNumber > Input.GetJoystickNames ().Length)
			ControllerNumber = Input.GetJoystickNames ().Length;
	}
	// Use this for initialization
	public int Action ()
	{
		if (ControllerNumber == 0)
			return 0;
		float value = Input.GetAxis ("L_XAxis_" + ControllerNumber);
		if (flipped) {			
			if (value < 0)
				return 1;
			if (value > 0)
				return -1;
		} else {
			if (value < 0)
				return -1;
			if (value > 0)
				return 1;
		}
		return 0;
	}

	public bool HeavyDown ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("Y_" + ControllerNumber);
	}
	public bool MediumDown ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("X_" + ControllerNumber);
	}
	public bool LightDown ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("A_" + ControllerNumber);
	}
	public bool BlockDown ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("B_" + ControllerNumber);
	}

	public bool Heavy ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButton ("Y_" + ControllerNumber);
	}
	public bool Medium ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButton ("X_" + ControllerNumber);
	}
	public bool Light ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButton ("A_" + ControllerNumber);
	}
	public bool Block ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButton ("B_" + ControllerNumber);
	}

	public bool Start ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("Start_" + ControllerNumber);
	}

	public bool Up ()
	{
		if (ControllerNumber == 0)
			return false;
		if (Input.GetAxis ("DPad_YAxis_" + ControllerNumber) > 0 || 
			Input.GetAxis ("L_YAxis_" + ControllerNumber) < 0) {
			if (upDown)
				return false;
			else
				upDown = true;
			return true;
		}
		upDown = false;
		return false;
	}

	public bool Down ()
	{
		if (ControllerNumber == 0)
			return false;
		if (Input.GetAxis ("DPad_YAxis_" + ControllerNumber) < 0 || 
			Input.GetAxis ("L_YAxis_" + ControllerNumber) > 0) {
			if (downDown)
				return false;
			else
				downDown = true;
			return true;
		}
		downDown = false;
		return false;
	}

	public bool Right ()
	{
		if (ControllerNumber == 0)
			return false;
		if (Input.GetAxis ("DPad_XAxis_" + ControllerNumber) > 0 || 
			Input.GetAxis ("L_XAxis_" + ControllerNumber) > 0) {
			if (rightDown)
				return false;
			else
				rightDown = true;
			return true;
		}
		rightDown = false;
		return false;
	}
	
	public bool Left ()
	{
		if (ControllerNumber == 0)
			return false;
		if (Input.GetAxis ("DPad_XAxis_" + ControllerNumber) < 0 || 
			Input.GetAxis ("L_XAxis_" + ControllerNumber) < 0) {
			if (leftDown)
				return false;
			else
				leftDown = true;
			return true;
		}
		leftDown = false;
		return false;
	}

	public bool Accept ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("A_" + ControllerNumber);
	}

	public bool Back ()
	{
		if (ControllerNumber == 0)
			return false;
		return Input.GetButtonDown ("B_" + ControllerNumber) || Input.GetButton ("Back_" + ControllerNumber);
	}
}
