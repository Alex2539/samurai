﻿using UnityEngine;
using System.Collections;

public class AIInput : Controller
{

	public void SetPlayer (int player)
	{
	}

	public void SetFlipped (bool flipped)
	{
	}

	public int Action ()
	{
		return 1;
	}

	public bool HeavyDown ()
	{
		return false;
	}

	public bool Heavy ()
	{
		return false;
	}

	public bool MediumDown ()
	{
		return false;
	}

	public bool Medium ()
	{
		return false;
	}

	public bool LightDown ()
	{
		return false;
	}

	public bool Light ()
	{
		return false;
	}

	public bool BlockDown ()
	{
		return false;
	}
	
	public bool Block ()
	{
		return false;
	}

	public bool Start ()
	{
		return false;
	}

	public bool Up ()
	{
		return false;
	}

	public bool Down ()
	{
		return false;
	}

	public bool Left ()
	{
		return false;
	}

	public bool Right ()
	{
		return false;
	}
	public bool Accept ()
	{
		return false;
	}

	public bool Back ()
	{
		return false;
	}

}
