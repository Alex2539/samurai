﻿using UnityEngine;
using System.Collections;

public class SoundLibrary : MonoBehaviour
{

	public AudioClip[] Hits;
	public AudioClip[] Clanks;
	public AudioClip[] HeavyAttack;
	public AudioClip[] MediumAttack;
	public AudioClip[] LightAttack;
	public AudioClip[] Unsheath;

	public AudioClip GetHit ()
	{
		return Hits [Random.Range (0, Hits.Length)];
	}
	public AudioClip GetClanks ()
	{
		return Clanks [Random.Range (0, Clanks.Length)];
	}
	public AudioClip GetHeavyAttack ()
	{
		return HeavyAttack [Random.Range (0, HeavyAttack.Length)];
	}
	public AudioClip GetMediumAttack ()
	{
		return MediumAttack [Random.Range (0, MediumAttack.Length)];
	}
	public AudioClip GetLightAttack ()
	{
		return LightAttack [Random.Range (0, LightAttack.Length)];
	}
	public AudioClip GetUnsheath ()
	{
		return Unsheath [Random.Range (0, Unsheath.Length)];
	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
