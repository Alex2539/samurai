﻿using UnityEngine;
using System.Collections;

public class Samurai : MonoBehaviour
{
	public enum SIDE
	{
		LEFT,
		RIGHT
	}
	public enum STATE
	{
		READY,
		HIT,
		STUN,
		ATTACK,
		BLOCK
	}
	public enum CONTROL
	{
		KEYBOARD,
		XBOX_CONTROLLER,
		AI
	}
	public SoundLibrary soundLibrary;
	public bool buttonA = false, buttonX = false, buttonY = false, buttonB = false;
	public SIDE side = SIDE.LEFT;
	// public CONTROL ControlMethod = CONTROL.XBOX_CONTROLLER;
	public int playerNumber = 0;
	public Controller controller;
	public int Health, MaxHealth = 10;
	public STATE state = STATE.READY;
	public Samurai Opponent;
	public float blockTimer = 0;
	public int Action = 0;

	private Animator anim;

	void PlaySound (AudioClip clip)
	{
		PlaySound (clip, 0, false);
	}
	void PlaySound (AudioClip clip, float delay)
	{
		PlaySound (clip, delay, false);
	}
	void PlaySound (AudioClip clip, float delay, bool shiftPitch)
	{
		audio.volume = GameSettings.GetInstance ().SoundVolume;
		audio.clip = clip;
		if (shiftPitch)
			audio.pitch = Random.Range (0.75f, 1.25f);
		else
			audio.pitch = 1;
		audio.PlayDelayed (delay);
	}

	void Start ()
	{
		Health = MaxHealth;
		anim = GetComponent<Animator> ();
		if (side == SIDE.RIGHT)
			transform.localScale = new Vector3 (-1, 1, 1);
	}

	public void SetController (Controller c)
	{
		controller = c;
		c.SetPlayer (playerNumber);
	}

	public int TryAttack (int strength)
	{
		if (state == STATE.HIT)
			return 0;
		if (blockTimer > 0) {
			PlaySound (soundLibrary.GetClanks ());
			if (blockTimer <= 0.5)
				return 2;
			return 0;
		} else {
			state = STATE.HIT;
			anim.SetInteger ("Action", 3);
			anim.SetBool ("Interruptable", false);
			Health -= strength;
			PlaySound (soundLibrary.GetHit ());
			return 1;
		}
	}

	/*
	void Update ()
	{
		float timeout = anim.GetFloat ("Timeout");
		if (timeout > 0)
			timeout -= Time.deltaTime;
		if (timeout < 0) {
			timeout = 0;
		}
		anim.SetFloat ("Timeout", timeout);

		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo (0);
		if (state == STATE.READY || anim.GetBool ("Interruptable")) {
			anim.SetBool ("CanStun", true);
			anim.SetBool ("CanHit", true);
			float action = controller.Action ();
			if (action > 0) {
				anim.SetInteger ("Action", 1);
				state = STATE.READY;
			} else if (action < 0) {
				anim.SetInteger ("Action", 2);
				anim.SetBool ("Interruptable", true);
				state = STATE.BLOCK;
			} else {
				anim.SetInteger ("Action", 0);	
				anim.SetBool ("Interruptable", false);
				blockTimer = 0;
				state = STATE.READY;
			}
			int weight = 0;
			if (controller.LightDown ()) {
				anim.SetInteger ("Weight", 1);
				weight = 1;
			} else if (controller.MediumDown ()) {
				anim.SetInteger ("Weight", 2);
				weight = 2;
			} else if (controller.HeavyDown ()) {
				anim.SetInteger ("Weight", 3);
				weight = 3;
			} else {
				anim.SetInteger ("Weight", 0);
			}

			if (action == 1 && weight > 0) {
				state = STATE.ATTACK;
				anim.SetBool ("Interruptable", false);
				if (weight == 1)
					PlaySound (soundLibrary.GetLightAttack (), 0f, true);
				if (weight == 2)
					PlaySound (soundLibrary.GetMediumAttack (), 0.25f, true);
				if (weight == 3)
					PlaySound (soundLibrary.GetHeavyAttack (), 0.4167f, true);
			}
		}

		if (state == STATE.BLOCK) {
			float action = controller.Action ();
			if (playerNumber == 1)
				Debug.Log (action);
			if (action < 0)
				blockTimer += Time.deltaTime;
			else {
				state = STATE.READY;
				blockTimer = 0;
			}
		}

		
		if (state == STATE.ATTACK && stateInfo.IsTag ("AttackPoint") && Opponent != null) {
			Debug.Log ("Hitting");
			int result = Opponent.TryAttack (anim.GetInteger ("Weight"));
			if (result == 2) {
				state = STATE.STUN;
				anim.SetInteger ("Action", 4);
				anim.SetBool ("Interruptable", false);
			} else {
				state = STATE.READY;
			}
		}

		if (state == STATE.HIT && stateInfo.IsName ("Idle")) {
			state = STATE.READY;
		}
		if (state == STATE.STUN && stateInfo.IsName ("Idle")) {
			state = STATE.READY;
		}

		
		
		if (stateInfo.IsName ("Stunned"))
			anim.SetBool ("CanStun", false);
		if (stateInfo.IsName ("GetHit"))
			anim.SetBool ("CanHit", false);

	}
	*/

	void Update ()
	{
		GameSettings settings = GameSettings.GetInstance ();
		if (settings.paused) {
			anim.speed = 0;
			return;
		} else {
			anim.speed = 1f;
		}
		float timeout = anim.GetFloat ("Timeout");
		if (timeout > 0)
			timeout -= Time.deltaTime;
		if (timeout < 0) {
			timeout = 0;
		}
		anim.SetFloat ("Timeout", timeout);
		
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo (0);
		float action = 0;
		if (state == STATE.READY || anim.GetBool ("Interruptable")) {
			anim.SetBool ("CanStun", true);
			anim.SetBool ("CanHit", true);

			int weight = 0;
			if (controller.Block ()) {
				anim.SetInteger ("Action", 2);
				anim.SetInteger ("Weight", 0);
				anim.SetBool ("Interruptable", true);
				weight = 3;
				action = -1;
				state = STATE.BLOCK;
				blockTimer += Time.deltaTime;
			} else {
				state = STATE.READY;
				blockTimer = 0;
				if (controller.Light ()) {
					anim.SetInteger ("Action", 1);
					anim.SetInteger ("Weight", 1);
					weight = 1;
					action = 1;
				} else if (controller.Medium ()) {
					anim.SetInteger ("Action", 1);
					anim.SetInteger ("Weight", 2);
					weight = 2;
					action = 1;
				} else if (controller.Heavy ()) {
					anim.SetInteger ("Action", 1);
					anim.SetInteger ("Weight", 3);
					weight = 3;
					action = 1;
				} else {
					anim.SetInteger ("Action", 0);
					anim.SetInteger ("Weight", 0);
				}

				if (action != 0)
					anim.SetBool ("Interruptable", false);
				else
					anim.SetBool ("Interruptable", true);
			}
			
			if (action > 0 && weight > 0) {
				state = STATE.ATTACK;
				anim.SetBool ("Interruptable", false);
				if (weight == 1)
					PlaySound (soundLibrary.GetLightAttack (), 0f, true);
				if (weight == 2)
					PlaySound (soundLibrary.GetMediumAttack (), 0.25f, true);
				if (weight == 3)
					PlaySound (soundLibrary.GetHeavyAttack (), 0.4167f, true);
			}
		}		
		
		if (state == STATE.ATTACK && stateInfo.IsTag ("AttackPoint") && Opponent != null) {
			int result = Opponent.TryAttack (anim.GetInteger ("Weight"));
			if (result == 2) {
				state = STATE.STUN;
				anim.SetInteger ("Action", 4);
				anim.SetBool ("Interruptable", false);
			} else {
				state = STATE.READY;
			}
		}
        
		if (state == STATE.HIT && stateInfo.IsName ("Idle")) {
			state = STATE.READY;
		}
		if (state == STATE.STUN && stateInfo.IsName ("Idle")) {
			state = STATE.READY;
		}
        
        
        
		if (stateInfo.IsName ("Stunned"))
			anim.SetBool ("CanStun", false);
		if (stateInfo.IsName ("GetHit"))
			anim.SetBool ("CanHit", false);
		
	}
}
