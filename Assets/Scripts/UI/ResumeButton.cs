﻿using UnityEngine;
using System.Collections;

public class ResumeButton : Button
{
	public PauseMenu pauseMenu;
	public override void OnClick ()
	{
		GameSettings.GetInstance ().paused = false;
		pauseMenu.Hide ();
	}

}
