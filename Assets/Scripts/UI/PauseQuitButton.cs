﻿using UnityEngine;
using System.Collections;

public class PauseQuitButton : Button
{

	public override void OnClick ()
	{
		GameSettings.GetInstance ().paused = false;
		Application.LoadLevel ("menu");
	}
}
