﻿using UnityEngine;
using System.Collections;

public class PauseMenu : Menu
{
	void Update ()
	{
		if (!Visible)
			return;
		if (firstUpdate) {
			firstUpdate = false;
			return;
		}
		if (keyIn.Up () || xboxIn.Up ()) {
			buttons [selected].state = Button.STATE.OFF;
			selected = (selected + buttons.Length - 1) % buttons.Length;
			buttons [selected].state = Button.STATE.OVER;
		}
			
		if (keyIn.Down () || xboxIn.Down ()) {
			buttons [selected].state = Button.STATE.OFF;
			selected = (selected + 1) % buttons.Length;
			buttons [selected].state = Button.STATE.OVER;
		}
			
		if (keyIn.Accept () || xboxIn.Accept ()) {
			buttons [selected].OnClick ();
		}
			
		if (keyIn.Back () || xboxIn.Back ()) {
			this.Hide ();
			GameSettings.GetInstance ().paused = false;
			Debug.Log ("Unpausing");
		}
	}
}
