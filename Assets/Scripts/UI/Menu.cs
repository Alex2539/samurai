﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
	protected KeyboardInput keyIn = new KeyboardInput ();
	protected X360Controller xboxIn = new X360Controller ();

	public Button[] buttons;
	public Menu PreviousMenu;
	public bool Visible = false;
	protected int selected = 0;
	protected bool firstUpdate = true;

	public void Hide ()
	{
		foreach (Button b in buttons) {
			b.collider2D.enabled = false;
			b.renderer.enabled = false;
			b.state = Button.STATE.OFF;
		}
		Visible = false;
		Debug.Log ("Hiding menu" + gameObject.name);
	}

	public void Show ()
	{
		foreach (Button b in buttons) {
			b.collider2D.enabled = true;
			b.renderer.enabled = true;
		}
		firstUpdate = true;
		selected = 0;
		buttons [0].state = Button.STATE.OVER;
		Visible = true;
		Debug.Log ("Showing menu" + gameObject.name);
	}

	// Use this for initialization
	void Start ()
	{
		xboxIn.SetPlayer (1);
		if (Visible)
			this.Show ();
		else
			this.Hide ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!Visible)
			return;
		if (firstUpdate) {
			firstUpdate = false;
			return;
		}
		if (keyIn.Up () || xboxIn.Up ()) {
			buttons [selected].state = Button.STATE.OFF;
			selected = (selected + buttons.Length - 1) % buttons.Length;
			buttons [selected].state = Button.STATE.OVER;
		}

		if (keyIn.Down () || xboxIn.Down ()) {
			buttons [selected].state = Button.STATE.OFF;
			selected = (selected + 1) % buttons.Length;
			buttons [selected].state = Button.STATE.OVER;
		}

		if (keyIn.Accept () || xboxIn.Accept ()) {
			buttons [selected].OnClick ();
		}

		if (keyIn.Back () || xboxIn.Back ()) {
			if (PreviousMenu != null) {
				this.Hide ();
				PreviousMenu.Show ();
			}

		}
	}
}
