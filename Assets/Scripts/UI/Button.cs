﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{

	public enum STATE
	{
		OFF,
		OVER,
		DOWN
	}
	public STATE state = STATE.OFF;
	public Sprite Off, Over, Down;
	private SpriteRenderer sr;

	public virtual void OnClick ()
	{
		Debug.Log ("No behaviour associated with button");
	}

	void Start ()
	{
		sr = GetComponent<SpriteRenderer> ();
	}

	void Update ()
	{
		if (Off != null && Over != null && Down != null) {
			switch (state) {
			case STATE.OFF:
				sr.sprite = Off;
				break;
			case STATE.OVER:
				sr.sprite = Over;
				break;
			case STATE.DOWN:
				sr.sprite = Down;
				break;
			}
		}
	}

	void OnMouseEnter ()
	{
		state = STATE.OVER;
	}
	void OnMouseExit ()
	{
		state = STATE.OFF;
	}
	void OnMouseDown ()
	{
		state = STATE.DOWN;
	}
	void OnMouseUpAsButton ()
	{
		OnClick ();
		state = STATE.OVER;
	}
}
