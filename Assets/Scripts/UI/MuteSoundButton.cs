﻿using UnityEngine;
using System.Collections;

public class MuteSoundButton : Button
{
	public SpriteRenderer checkbox;
	public Sprite check;
	public Sprite uncheck;

	public override void OnClick ()
	{
		GameSettings settings = GameSettings.GetInstance ();
		settings.SoundVolume = -settings.SoundVolume + 1;
		if (settings.SoundVolume == 0) {
			checkbox.sprite = uncheck;
		} else {
			checkbox.sprite = check;
		}
	}
}
