﻿using UnityEngine;
using System.Collections;

public class ChangeMenuButton : Button
{
	public Menu CurrentMenu;
	public Menu OtherMenu;

	public override void OnClick ()
	{
		CurrentMenu.Hide ();
		OtherMenu.Show ();
	}
}
