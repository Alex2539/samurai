﻿using UnityEngine;
using System.Collections;

public class MuteMusicButton : Button
{

	public SpriteRenderer checkbox;
	public Sprite check;
	public Sprite uncheck;
	
	public override void OnClick ()
	{
		GameSettings settings = GameSettings.GetInstance ();
		settings.MusicVolume = -settings.MusicVolume + 1;
		if (settings.MusicVolume == 0) {
			checkbox.sprite = uncheck;
		} else {
			checkbox.sprite = check;
		}
	}
}
