﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
	private static GameSettings instance;
	
	public static GameSettings GetInstance ()
	{
		if (instance == null) {
			GameObject go = new GameObject ();
			instance = go.AddComponent<GameSettings> ();
		}
		return instance;
	}

	public bool paused = false;
	public Samurai.CONTROL Player1Control;
	public Samurai.CONTROL Player2Control;
	public float SoundVolume = 1;
	public float MusicVolume = 1;

	// Use this for initialization
	void Start ()
	{
		if (GameSettings.instance == null) {
			GameSettings.instance = this;
			DontDestroyOnLoad (this);
		} else
			DestroyImmediate (this);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
}
